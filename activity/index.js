/////////////////////////////////////////////////
// Activity 1
/////////////////////////////////////////////////

let number = Number(prompt("Give me a number"));

console.log('The number you provided is ' + number);

do {
    if ( (number%10) == 0) {
        console.log('The number is divisible by 10. Skipping the number');
    } else if ( (number%5) == 0) {
        console.log(number);
    }
    number--;
} while (number >= 50)
console.log('The current value is at 50. Terminating the loop');


/////////////////////////////////////////////////
// Activity 2
/////////////////////////////////////////////////
let string = prompt("Give me a string");
var newString = ''
let vowels = ['a', 'e', 'i', 'o', 'u'];

for (var i = 0; i < string.length; i++) {
  if ( !vowels.includes(string[i]) ) {
      newString = newString + string[i];
  }
}
console.log(newString);